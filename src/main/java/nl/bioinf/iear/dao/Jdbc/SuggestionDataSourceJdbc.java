package nl.bioinf.iear.dao.Jdbc;

import nl.bioinf.iear.dao.SuggestionDAO;
import nl.bioinf.iear.model.Suggestion;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by rHut on 05-6-18.
 * Handles all suggestion related database queries.
 */

@Component
public class SuggestionDataSourceJdbc implements SuggestionDAO{
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public SuggestionDataSourceJdbc(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    /**
     * Returns all suggestions in a list
     * @return List<Suggestion> list
     */
    @Override
    public List<Suggestion> getAllSuggestions() {
        String sql = "SELECT * FROM suggestions";
        return jdbcTemplate.query(sql, new suggestionRowMapper());
    }

    /**
     * Returns a list of all suggestions with a specific type
     * @param suggestionType String suggestion type
     * @return List<Suggestion> list
     */
    @Override
    public List<Suggestion> getAllSuggestionsByType(String suggestionType) {
        String sql = "SELECT * FROM suggestions WHERE suggestion_type = '" + suggestionType + "'";
        return jdbcTemplate.query(sql, new suggestionRowMapper());
    }

    /**
     * Returns a suggestion with a certain id
     * @param id suggestionId
     * @return Suggestion object
     */
    @Override
    public Suggestion getSuggestionById(int id) {
        return namedParameterJdbcTemplate.queryForObject("SELECT * FROM suggestions WHERE suggestion_id =:id;",
                new MapSqlParameterSource("id", id),
                new suggestionRowMapper());
    }

    /**
     * Inserts the suggestion into the database
     * @param suggestion object
     * @return Suggestion object
     */
    @Override
    public Suggestion insertSuggestion(Suggestion suggestion) {
        String sql = "INSERT INTO suggestions(suggestion_type, suggestion_text) VALUES(?, ?);";
        jdbcTemplate.update(sql, suggestion.getSuggestionType(), suggestion.getSuggestionText());
        return suggestion;
    }

    /**
     * Deletes a suggestion with a certain id
     * @param id suggestionId
     * @return boolean boolean
     */
    @Override
    public boolean deleteSuggestion(int id) {
        String sql = "DELETE FROM suggestions WHERE suggestion_id = '" + id + "';";
        jdbcTemplate.update(sql);
        return true;

    }

    private class suggestionRowMapper implements RowMapper<Suggestion> {
        /**
         * Returns a suggestion object filled with the row data
         * @param rs Resultset
         * @param rowNum RowNumber
         * @return Suggestion object
         * @throws SQLException exception
         */
        @Override
        public Suggestion mapRow(ResultSet rs, int rowNum) throws SQLException {
            int id = rs.getInt("suggestion_id");
            String suggestionType = rs.getString("suggestion_type");
            String suggestionText = rs.getString("suggestion_text");
            String timeStamp = rs.getString("ts");

            return new Suggestion(id, suggestionType, suggestionText, timeStamp);
        }
    }
}
