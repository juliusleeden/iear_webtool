package nl.bioinf.iear.service;

import nl.bioinf.iear.model.Antibody;
import nl.bioinf.iear.model.Antigen;
import nl.bioinf.iear.model.Supplier;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class AntibodyServiceTest {
    private Antigen testAntigen = new Antigen("TestAntigen", "Testing", "TA");
    private Supplier testSupplier = new Supplier(1);
    private Antibody testAntibody = new Antibody("TT-1234", "T", testSupplier, testAntigen, "TestingCreature");

    @Autowired
    AntibodyService antibodyService;

    @Before
    public void setUp() {
        antibodyService.insertAntibody(testAntibody);}

    @After
    public void tearDown() {
        antibodyService.deleteAntibody(antibodyService.getAntibodyByCatalogCode(testAntibody.getCatalogCode()).getId());}

    @Test
    public void notNullListAntibodies() {
        List<Antibody> antibodies = antibodyService.listAntibodies();
        assertNotNull(antibodies);
    }

    @Test
    public void getAllAntibodiesByIsotypeSunny(){
        List<Antibody> antibodies = antibodyService.getAllAntibodiesByIsotype("T");
        assertNotNull(antibodies);
    }

    @Test (expected = BadSqlGrammarException.class)
    public void getAllAntibodiesByIsotypeBoundary(){
        List<Antibody> antibodies = antibodyService.getAllAntibodiesByIsotype("Doesn't exist");
    }

    @Test
    public void getAntibodyByIdSunny(){
        Antibody antibody = antibodyService.getAntibodyById(1);
        int expected = 1;
        assertEquals(expected, antibody.getId());
    }

    @Test (expected = EmptyResultDataAccessException.class)
    public void getAntibodyByNotExistingId(){
        Antibody antibody = antibodyService.getAntibodyById(230920922);
    }

    @Test (expected = EmptyResultDataAccessException.class)
    public void getAntibodyByIdBoundary(){
        Antibody antibody = antibodyService.getAntibodyById(0);
    }

    @Test
    public void getAntibodyByCatalogCodeSunny(){
        String expected = "Antibody{id=1, name='-', catalogCode='sc-74516', isotype='G2A', supplier=TestSupplier{id=1, name='SCBT', contactLink='https://www.scbt.com/'}, " +
                "antigen=Antigen{id=1, name='Myosin VIIa', description='Amino acids 11-70 mapping near the n-terminus of human Myosin VIIA', geneSymbol='MYO7A'}, " +
                "host='Mouse', performanceComments='-', applications=WB, structureLabelingNote='IHC, OHC'}";
        Antibody actual = antibodyService.getAntibodyByCatalogCode("sc-74516");
        assertEquals(expected, actual.toString());
    }

    @Test (expected = EmptyResultDataAccessException.class)
    public void getAntibodyByCatalogCodeBoundary(){
        Antibody actual = antibodyService.getAntibodyByCatalogCode("Doesn't exist");
    }

    @Test
    public void insertAntibodySunny(){
        String expected = "TT-1234";
        String actual = antibodyService.getAntibodyByCatalogCode("TT-1234").getCatalogCode();
        assertEquals(expected, actual);
    }

    @Test (expected = NullPointerException.class)
    public void insertAntibodyNull(){
        antibodyService.insertAntibody(null);
    }

    @Test
    public void updateAntibodySunny(){
        String expected = antibodyService.getAntibodyByCatalogCode("TT-1234").toString();
        antibodyService.updateAntibody(testAntibody);
        String result = antibodyService.getAntibodyByCatalogCode("TT-1234").toString();
        assertEquals(expected, result);
    }

    @Test (expected = NullPointerException.class)
    public void updateAntibodyBoundary() {
        antibodyService.updateAntibody(null);
    }

    @Test
    public void deleteAntibodyBoundary(){
        antibodyService.deleteAntibody(10000000);
    }
}