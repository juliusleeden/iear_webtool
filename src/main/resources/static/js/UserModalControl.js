/**
 * Name: ModalControl
 * Version: 0.2.1.8.6.04
 * Author: JvdLeeden, RgHut
 * Use: modal functioning and ajax post requests
 */

$(document).ready(function () {
    // Retrieves table data
    var table = Alltables.table;
    var tableBody = Alltables.tableBody;

    // Saves all the user names for later modal verification.
    var userList = [];
    $.getJSON('/data/users', function (data) {
        $.each(data, function (key, entry) {
            userList.push(entry.username);
        });
    });

    // Starts the delete modal.
    tableBody.on('click', 'td.delete', function () {
        var tr = $(this).closest('tr');
        if (table.row(tr).data().username !== '-') {
            $('#userDeleteModal').data('tr', tr).modal();
        } else {
            alert("entry already deleted");
        }
    });

    // Confirms the delete and sends the user to be deleted to /data/users/deleteUser.
    $('#deleteYes').click(function () {
        var modal = $('#userDeleteModal');
        var tr = modal.data('tr');
        var data = table.row(tr).data().username;

        //Handy log to show the data before sending it.
        //console.log("Data before send: ", data);

        // Handles the ajax request
        var href = "/data/users/deleteUser";
        $.ajax({
            url: href,
            type: "POST",
            contentType: 'application/json',
            dataType: 'json',
            data: data,
            success: function (result) {
                console.log("SUCCESS:", result);

                //changes the data in the row to show it has been deleted
                var deletedData = {
                    username: '-',
                    role: 'Entry deleted'
                };
                table.row(tr).data(deletedData);
                table.draw();
            },
            error: function (e) {
                console.log("ERROR:", e);
            }
        });
        modal.modal('hide');
    });

    //New and edit modal share the same modal template, but change parts of the input and output.
    // starts the new modal.
    $('#new_user').on('click', function () {
        var edit = $('#userEditModal');
        edit.data('tr', 0).modal();
    });

    // Starts the edit modal.
    tableBody.on('click', 'td.edit', function () {
        var tr = $(this).closest('tr');
        var edit = $('#userEditModal');

        //If the row was already deleted say so.
        if (table.row(tr).data().username !== '-') {
            //Retrieves the user info, disables the username attribute as this is not supposed to be changed.
            edit.find('[name="username"]').val(table.row(tr).data().username);
            edit.find('[name="userRole"]').val(table.row(tr).data().role);
            $('#userName').attr('disabled', true);
            edit.data('tr', tr).modal();
        } else {
            alert("entry already deleted");
        }
    });

    // Confirms the Edit/New user and sends the information to /data/users/insertUser or /data/users/editUser depending on the modal.
    $('#editConfirm').click(function () {
        var edit = $('#userEditModal');

        // retrieves the information destination.
        var location = "insertUser";
        if (edit.data('tr') !== 0) {
            location = "editUser";
        }

        // retrieves all of the user information from the modal.
        var username = edit.find('[name="username"]').val();
        var role = edit.find('[name="userRole"]').val();
        var password = edit.find('[name="password"]').val();
        var confirmPassword = edit.find('[name="confirmPassword"]').val();
        var errorMessage = "";

        // Checks which path to take and whether all of the information criteria has been met.
        if(!((location === "insertUser") && (password === ""))) {
            if ((password === confirmPassword) && (username !== "") && (role !== null) && (!userList.includes(username) || location === "editUser")) {

                // Sets the data to JSON format.
                var userData = {
                    username: username,
                    role: role,
                    password: password
                };

                //Handy log to show the data before sending it.
                //console.log("data before send: ", userData);

                //Handles the ajax request. Sending the user information to editUser or newUser.
                var href = "/data/users/" + location;
                $.ajax({
                    url: href,
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: 'json',
                    data: JSON.stringify(userData),
                    success: function (result) {
                        var modal = $('#messageModal');
                        modal.find('.modal-body').text(result);
                        modal.modal();
                    },
                    error: function (data, status, er) {
                        alert("error: " + data + " status: " + status + " er: " + er)
                    }
                });
                edit.modal('hide');

                // If statements allowing for a dynamic error message. Will change depending on what is wrong.
            } else {
                if ((userList.includes(username) && (location === "insertUser"))) {
                    errorMessage = "This username already exists!";
                } else {
                    errorMessage = "Please insert a username!"
                }
                edit.find($('#passwordMatch')).text("Passwords don't match!");
                edit.find($('#addUserName')).text(errorMessage);
                edit.find($('#addRole')).text("Please Choose a role!");
            }
        } else {
            if ((userList.includes(username) && (location === "insertUser"))) {
                errorMessage = "This username already exists!";
            } else {
                errorMessage = "Please insert a username!"
            }
            edit.find($('#addUserName')).text(errorMessage);
            edit.find($('#passwordMatch')).text("Password can't be empty");
            edit.find($('#addRole')).text("Please Choose a role!");
        }

        // If these fields are filled in, remove the error message.
        if ((username !== "") && (!userList.includes(username))) {
            edit.find($('#addUserName')).text("");
        }
        if((password === confirmPassword) && (password !== "")){
            edit.find($('#passwordMatch')).text("");
        }
        if (role !== null) {
            edit.find($('#addRole')).text("");
        }

    });

    // Reloads the table and empties the modal values when closing the modal.
    $('#userEditModal').on('hidden.bs.modal', function () {

        table.ajax.reload();
        var edit = $('#userEditModal');

        // Sets the modal fields to basic.
        edit.find('[name="username"]').val('');
        edit.find('[name="userRole"]').val('Select user role*');
        edit.find('[name="password"]').val('');
        edit.find('[name="confirmPassword"]').val('');
        edit.data('tr', 0);

        //Empties the error fields.
        edit.find($('#addUserName')).text("");
        edit.find($('#passwordMatch')).text("");
        edit.find($('#addRole')).text("");

        //Disables the User name form. (should not be accessible for edit user).
        $('#userName').attr('disabled', false);
    });
});
