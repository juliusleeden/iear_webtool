package nl.bioinf.iear.service;

import nl.bioinf.iear.dao.AntibodyDAO;
import nl.bioinf.iear.model.Antibody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jpvanderleeden on 18-3-18.
 * Service that implements AntibodyDataSourceJdbc.
 */

@Service
public class AntibodyService {
    private final AntibodyDAO antibodyDAO;

    @Autowired
    public AntibodyService(AntibodyDAO antibodyDAO) { this.antibodyDAO = antibodyDAO; }

    public List<Antibody> listAntibodies() {
            return antibodyDAO.getAllAntibodies();
    }

    public List<Antibody> getAllAntibodiesByIsotype(String isotype) { return antibodyDAO.getAllAntibodiesByIsotype(isotype); }

    public Antibody getAntibodyById(int antibodyId) { return antibodyDAO.getAntibodyById(antibodyId); }

    public Antibody getAntibodyByCatalogCode(String catalogCode) { return antibodyDAO.getAntibodyByCatalogCode(catalogCode); }

    public void deleteAntibody(int antibodyId) { antibodyDAO.deleteAntibody(antibodyId); }

    public void insertAntibody(Antibody antibody) { antibodyDAO.insertAntibody(antibody); }

    public void updateAntibody(Antibody antibody) { antibodyDAO.updateAntibody(antibody); }
}
