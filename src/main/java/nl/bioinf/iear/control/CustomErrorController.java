package nl.bioinf.iear.control;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;


/**
 * Created by jpvanderleeden on 10-6-18.
 * CustomErrorContoller, Controls all requests that do not exist or have no permission and shows the appropriate erorr code.
 */

@Controller
public class CustomErrorController implements ErrorController{

    @RequestMapping("/error")
    @ExceptionHandler()
    public String handleError(Model model, HttpServletRequest request){
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null){
            Integer statusCode = Integer.valueOf(status.toString());

            if (statusCode == HttpStatus.BAD_REQUEST.value()) {
                model.addAttribute("errorMessage", "error 400: BAD REQUEST");
            }

            else if (statusCode == HttpStatus.UNAUTHORIZED.value()){
                model.addAttribute("errorMessage", "error 401: UNAUTHORIZED");
            }

            else if (statusCode == HttpStatus.FORBIDDEN.value()){
                model.addAttribute("errorMessage", "error 403: FORBIDDEN");
            }

            else if (statusCode == HttpStatus.NOT_FOUND.value()){
                model.addAttribute("errorMessage", "error 404: NOT FOUND");
            }

            else if (statusCode == HttpStatus.METHOD_NOT_ALLOWED.value()) {
                model.addAttribute("errorMessage", "error 405: METHOD NOT ALLOWED");
            }
            else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()){
                model.addAttribute("errorMessage", "error 500: INTERNAL SERVER ERROR");
            }
        }
        return "error";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
