package nl.bioinf.iear.dao.Jdbc;

import nl.bioinf.iear.dao.HistoryDAO;
import nl.bioinf.iear.model.History;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by jpvanderleeden on 15-6-18.
 * Handles all history related database queries.
 */

@Component
public class HistoryDataSourceJdbc implements HistoryDAO {

    private final JdbcTemplate jdbcTemplate;

    public HistoryDataSourceJdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Inserts a new history message into the database
     * @param history Object
     * @return History Object
     */
    @Override
    public History insertHistoryMessage(History history) {
        String sql = "INSERT INTO history (user_name, message) VALUES (?, ?);";
        jdbcTemplate.update(sql, history.getUsername(), history.getMessage());
        return history;
    }

    /**
     * returns a list of all history objects
     * @return List<History> list
     */
    @Override
    public List<History> getAllHistoryMessages() {
        String sql = "SELECT * FROM history ORDER BY ts DESC;";
        return jdbcTemplate.query(sql, new historyRowMapper());

    }

    /**
     * Clears history messages older than 14 days
     * @return boolean boolean
     */
    @Override
    public boolean clearOldHistoryMessages() {
        String sql = "DELETE FROM history WHERE ts < (CURRENT_TIMESTAMP - INTERVAL 14 DAY)";
        jdbcTemplate.update(sql);
        return true;
    }


    private class historyRowMapper implements RowMapper<History> {

        /**
         * Returns history object filled with the row data
         * @param rs Resultset
         * @param rowNum RowNumber
         * @return History object
         * @throws SQLException exception
         */
        @Override
        public History mapRow(ResultSet rs, int rowNum) throws SQLException {
            int id = rs.getInt("history_id");
            String userName = rs.getString("user_name");
            String message = rs.getString("message");
            String timeStamp = rs.getString("ts");

            return new History(id, userName, message, timeStamp);
        }
    }
}
