/**
 * Name: ImageMapControl
 * Version: 0.4.1.8.6.13
 * Author: Rghut
 * Use: Inner-ear schematic interactivity
 */

$(document).ready(function () {
    var table = Alltables.table;

    //When this are is pressed on the schematic, sort antibodies by structure on: OC
    $('#ocArea').click(function () {
        console.log(structureColumn);
        table.search('').columns().search('').draw();
        table.column(structureColumn).search("OC").draw();
    });

    //When this are is pressed on the schematic, sort antibodies by structure on: Vestibule
    $('#vestibuleArea').click(function () {
        console.log(structureColumn);
        table.search('').columns().search('').draw();
        table.column(structureColumn).search("Vestibule").draw();
    });

});