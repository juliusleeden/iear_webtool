/**
 * Name: ModalControl
 * Version: 0.3.1.8.6.04
 * Author: RgHut
 * Use: modal functioning and ajax post requests
 */

$(document).ready(function () {
    // Retrieves table data
    var table = Alltables.table;
    var tableBody = Alltables.tableBody;

    //Saves all the catalog codes, used in duplicate catalog code validation.
    var antibodyList = [];
    $.getJSON('/data/antibodies', function (data) {
        $.each(data, function (key, entry) {
            antibodyList.push(entry.catalogCode);
        });
    });

    //Creates a dropdown menu filled with all supplier names.
    var dropdown = $('#supplier-dropdown');
    dropdown.empty();
    dropdown.append('<option selected="selected" disabled>Choose Supplier*</option>');
    dropdown.prop('selectedIndex', 0);
    const url = '/data/suppliers';
    $.getJSON(url, function (data) {
        $.each(data, function (key, entry) {
            dropdown.append($('<option></option>').attr('value', entry.id).text(entry.name));
        })
    });

    // Starts the edit modal
    $('#new_antibody').on('click', function () {
        $('#editModal').data('id', 0).modal();

    });

    // Starts the delete modal.
    tableBody.on('click', 'td.delete', function () {
        var tr = $(this).closest('tr');
        if (table.row(tr).data().id !== 0) {
            $('#deleteModal').data('tr', tr).modal();
        } else {
            alert("entry already deleted");
        }
    });

    // Retrieves all data from the table row.
    tableBody.on('click', 'td.edit', function () {
        var tr = $(this).closest('tr');
        if (table.row(tr).data().id !== 0) {
            var edit = $('#editModal');

            //Spots for showing incomplete forms.
            edit.find($('#addSupplier')).text("");
            edit.find($('#addCatalogCode')).text("");

            // Retrieves the data from row and add it to the modal forms. (for edit modals.)
            edit.find('[name="antibodyName"]').val(table.row(tr).data().name);
            edit.find('[name="catalogCode"]').val(table.row(tr).data().catalogCode);
            edit.find('[name="isotype"]').val(table.row(tr).data().isotype);
            edit.find('[name="antigenName"]').val(table.row(tr).data().antigen.name);
            edit.find('[name="antigenDescription"]').val(table.row(tr).data().antigen.description);
            edit.find('[name="gene"]').val(table.row(tr).data().antigen.geneSymbol);
            edit.find('[name="host"]').val(table.row(tr).data().hostOrganism);
            edit.find('[name="application"]').val(table.row(tr).data().applications);
            edit.find('[name="structure"]').val(table.row(tr).data().structureLabelingNote);
            edit.find('[name="supplier"]').val(table.row(tr).data().supplier.id);
            edit.find('[name="performance"]').val(table.row(tr).data().performanceComments);
            edit.data('id', table.row(tr).data().id).modal();
        } else {
            alert("entry already deleted");
        }
    });

    // Sends data to controller, location is dependant on the modal.
    $('#editConfirm').click(function () {
        var edit = $('#editModal');
        var id = 0;
        var location = "insertAntibody";
        if (edit.data('id') !== 0) {
            id = edit.data('id');
            location = "editAntibody"
        }

        //Saves all the data from the rows of the modal.
        var antibodyName = edit.find('[name="antibodyName"]').val();
        var catalogCode = edit.find('[name="catalogCode"]').val();
        var isotype = edit.find('[name="isotype"]').val();
        var antigenName = edit.find('[name="antigenName"]').val();
        var antigenDescription = edit.find('[name="antigenDescription"]').val();
        var gene = edit.find('[name="gene"]').val();
        var host = edit.find('[name="host"]').val();
        var application = edit.find('[name="application"]').val();
        var supplier = edit.find('[name="supplier"]').val();
        var performance = edit.find('[name="performance"]').val();
        var structure = edit.find('[name="structure"]').val();

        //Validates catalogCode, supplier and location. Location because edit modals are able to send duplicate names but new modals can not.
        if ((catalogCode !== "-" && catalogCode !== "") && (supplier !== null) && (!antibodyList.includes(catalogCode) || location === "editAntibody")) {

            //Save the data in JSON format.
            var data2 = {
                id: id,
                name: antibodyName,
                catalogCode: catalogCode,
                isotype: isotype,
                hostOrganism: host,
                applications: application,
                structureLabelingNote: structure,
                performanceComments: performance,
                antigen: {
                    name: antigenName,
                    description: antigenDescription,
                    geneSymbol: gene
                },
                supplier: {
                    id: supplier[0]
                }
            };

            //Handy console.log to check if the data is alright before sending.
            //console.log("data before send ", data2);

            //Ajax post request to the controller (/data/insertAntibody or /data/editAntibody)
            var href = "/data/" + location;
            $.ajax({
                url: href,
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(data2),
                success: function (result) {
                    var modal = $('#messageModal');
                    modal.find('.modal-body').text(result);
                    modal.modal();
                },
                error: function (data, status, er) {
                    alert("error: " + data + " status: " + status + " er: " + er)
                }
            });
            edit.modal('hide');

        } else {
            //These if statements allow for a dynamic view. It shows which parts of the modal are not filled in correctly.
            if ((antibodyList.includes(catalogCode) && (location === "insertAntibody"))) {
                var errormessage = "This catalogcode already exists!";
            } else {
                errormessage = "Please insert a catalog code!"
            }
            edit.find($('#addCatalogCode')).text(errormessage);
            edit.find($('#addSupplier')).text("Please choose a Supplier!");
        }

        // When a form has been filled, remove the error.
        if (supplier !== null) {
            edit.find($('#addSupplier')).text("");
        }

        if ((catalogCode !== "") && (!antibodyList.includes(catalogCode))) {
            edit.find($('#addCatalogCode')).text("");
        }
    });


    // Empties modal and reloads the table when closing the modal.
    $('#editModal').on('hidden.bs.modal', function () {
        table.ajax.reload();
        var modal = $('#editModal');

        modal.find('[name="antibodyName"]').val('');
        modal.find('[name="catalogCode"]').val('');
        modal.find('[name="isotype"]').val('');
        modal.find('[name="antigenName"]').val('');
        modal.find('[name="antigenDescription"]').val('');
        modal.find('[name="gene"]').val('');
        modal.find('[name="host"]').val('');
        modal.find('[name="application"]').val('');
        modal.find('[name="structure"]').val('');
        modal.find('[name="supplier"]').val('Choose Supplier*');
        modal.find('[name="performance"]').val('');
        modal.data('id', 0);
    });

    //Confirms the delete and sends the ID of the to be deleted antibody to /data/confirmDelete.
    $('#deleteYes').click(function () {
        var modal = $('#deleteModal');
        var tr = modal.data('tr');
        var id = table.row(tr).data().id;
        var href = "/data/confirmDelete/" + id;
        $.ajax({
            type: "POST",
            contentType: false,
            url: href,
            success: function (result) {
                var modal = $('#messageModal');
                modal.find('.modal-body').text(result);
                modal.modal();
            },
            error: function (e) {
                console.log("ERROR:", e);
            }
        });
        modal.modal('hide');

        //Row data will be replaced with this data to show that the row has been deleted.
        var data = {
            id: 0,
            name: '-',
            catalogCode: table.row(tr).data().catalogCode,
            isotype: '-',
            hostOrganism: '-',
            applications: '-',
            structureLabelingNote: '-',
            performanceComments: '-',
            antigen: {
                name: 'entry',
                description: '-',
                geneSymbol: 'deleted'
            },
            supplier: {
                id: '-',
                name: '-'
            }
        };
        table.row(tr).data(data);
        table.draw();
    });
});