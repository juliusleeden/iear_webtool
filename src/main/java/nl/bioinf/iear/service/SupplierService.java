package nl.bioinf.iear.service;

import nl.bioinf.iear.dao.SupplierDAO;
import nl.bioinf.iear.model.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jpvanderleeden on 9-5-18.
 * Service for SupplierDataSourceJdbc.
 */

@Service
public class SupplierService {
    private final SupplierDAO supplierDAO;

    @Autowired
    public SupplierService(SupplierDAO supplierDAO) {this.supplierDAO = supplierDAO; }

    public List<Supplier> listSuppliers() { return supplierDAO.listSuppliers(); }

    public Supplier getSupplierById(int supplierId) { return supplierDAO.getSupplierById(supplierId); }

    public int getSupplierIdByName(String supplierName) { return supplierDAO.getSupplierIdByName(supplierName); }

    public void insertSupplier(Supplier supplier) { supplierDAO.insertSupplier(supplier); }
}
