package nl.bioinf.iear.dao;

import nl.bioinf.iear.model.History;

import java.util.List;

/**
 * Created by jpvanderleeden on 07-06-18
 * Interface for HistoryDataSourceJdbc.
 */

public interface HistoryDAO {

    History insertHistoryMessage(History history);

    List<History> getAllHistoryMessages();

    boolean clearOldHistoryMessages();
}
