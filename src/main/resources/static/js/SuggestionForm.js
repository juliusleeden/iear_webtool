/**
 * Name: ModalControl
 * Version: 0.2.1.8.6.07
 * Author: RgHut
 * Use: Form control for guest users to suggest changes to the database
 */

$(document).ready(function () {

    //Loads the suggestion modal, which contains a random Captcha code.
    $('#suggest').on('click', function () {
        var suggest = $('#suggestModal');

        //Spots for showing incomplete forms.
        suggest.find($('#addSuggestionType')).text("");
        suggest.find($('#addSuggestionText')).text("");
        suggest.find($('#wrongCaptcha')).text("");

        //Generates 5 random numbers for the Captcha code.
        var a = Math.ceil(Math.random() * 9) + '';
        var b = Math.ceil(Math.random() * 9) + '';
        var c = Math.ceil(Math.random() * 9) + '';
        var d = Math.ceil(Math.random() * 9) + '';
        var e = Math.ceil(Math.random() * 9) + '';

        var code = a + b + c + d + e;
        suggest.find('[name="CaptchaCode"]').val(code);
        document.getElementById("CaptchaDiv").innerHTML = code;
        suggest.modal();
    });

    //Confirms the suggestion and sends it to /suggestion/insert
    $('#confirmSuggestion').click(function () {
        var suggest = $('#suggestModal');

        //obtains the values.
        var suggestionType = suggest.find('[name="suggestionType"]').val();
        var suggestionText = suggest.find('[name="suggestionText"]').val();

        //Checks if all forms have been filled in.
        if ((checkForm(suggest)) && (suggestionType !== null) && (suggestionText !== "")) {
            var data = {
                suggestionType: suggestionType,
                suggestionText: suggestionText
            };

            //Handy log that shows the data before sending it.
            // console.log("data before send ", data);

            //Handles the ajax request.
            var href = "/suggestion/insert";
            $.ajax({
                url: href,
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(data),
                success: function (result) {
                    var modal = $('#messageModal');
                    modal.find('.modal-body').text(result);
                    modal.modal();
                },
                error: function (data, status, er) {
                    console.log("error", data, status, er)
                }
            });
            suggest.modal('hide');

            //These if statements allow for a dynamic view. It shows which parts of the modal or not filled in correctly.
        } else {
            suggest.find($('#addSuggestionType')).text("Please choose a suggestion type!");
            suggest.find($('#addSuggestionText')).text("Please type a suggestion!");
            suggest.find($('#wrongCaptcha')).text("Wrong captcha code!");
        }

        //Empties the error spots when conditions have been met, giving a more dynamic feel.
        if (suggestionType !== null) {
            suggest.find($('#addSuggestionType')).text("");
        }

        if (suggestionText !== "") {
            suggest.find($('#addSuggestionText')).text("");
        }

        if (checkForm(suggest)) {
            suggest.find($('#wrongCaptcha')).text("");
        }
    });

    //Empties the modal values on closing.
    $('#suggestModal').on('hidden.bs.modal', function () {
        var modal = $('#suggestModal');
        modal.find('[name="suggestionType"]').val('Suggestion Type*');
        modal.find('[name="suggestionText"]').val('');
        modal.find('[name="CaptchaInput"]').val('');
    });

    // Captcha Script, handles the captcha validation.
    function checkForm(modal) {
        var errorMessage = "";

        // Retrieves the data from the forms.
        var captchaCode = modal.find('[name="CaptchaCode"]').val();
        var captchaInput = modal.find('[name="CaptchaInput"]').val();

        if (captchaInput === "") {
            errorMessage += "- Please Enter CAPTCHA Code.\n";
        }

        if (captchaInput !== "") {
            //checks whether the captcha code and the input code are equal.
            captchaCode= captchaCode.split(' ').join('');
            captchaInput = captchaInput.split(' ').join('');
            if (captchaCode !== captchaInput) {
                errorMessage += "- The CAPTCHA Code Does Not Match.\n";
            }
        }
        return errorMessage === "";
    }
});