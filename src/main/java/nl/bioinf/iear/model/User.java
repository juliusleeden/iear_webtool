package nl.bioinf.iear.model;

/**
 * Created by rHut on 19-4-18.
 * POJO for User objects
 */

public class User {
    private String username;
    private String password;
    private String role;

    public User(String userName, String role) {
        this.username = userName;
        this.role = role;
    }

    public User(String userName, String password, String role) {
        this.username = userName;
        this.password = password;
        this.role = role;
    }

    public User(){ }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() { return role; }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
