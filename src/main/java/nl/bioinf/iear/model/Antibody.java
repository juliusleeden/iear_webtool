package nl.bioinf.iear.model;

/**
 * Created by jpvanderleeden on 15-3-18.
 * POJO for antibody objects
 */

public class Antibody {
    private String catalogCode;
    private String isotype;
    private String hostOrganism;

    private int id;
    private String name;
    private String performanceComments;
    private String applications;
    private String structureLabelingNote;

    private Antigen antigen;
    private Supplier supplier;

    public Antibody(String catalogCode, String isotype, Supplier supplier, Antigen antigen, String hostOrganism) {
        this.catalogCode = catalogCode;
        this.isotype = isotype;
        this.supplier = supplier;
        this.antigen = antigen;
        this.hostOrganism = hostOrganism;
    }

    public Antibody(String catalogCode, String isotype, Supplier supplier, Antigen antigen, String host, int id) {
        this.catalogCode = catalogCode;
        this.isotype = isotype;
        this.supplier = supplier;
        this.antigen = antigen;
        this.hostOrganism = host;
        this.id = id;
    }

    public Antibody(){ }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCatalogCode() {
        return catalogCode;
    }

    public String getIsotype() {
        return isotype;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public Antigen getAntigen() {
        return antigen;
    }

    public String getHostOrganism() {
        return hostOrganism;
    }

    public String getPerformanceComments() {
        return performanceComments;
    }

    public String getApplications() { return applications; }

    public String getStructureLabelingNote() {
        return structureLabelingNote;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPerformanceComments(String performanceComments) {
        this.performanceComments = performanceComments;
    }

    public void setApplications(String applications) { this.applications = applications; }

    public void setStructureLabelingNote(String structureLabelingNote) {
        this.structureLabelingNote = structureLabelingNote;
    }

    @Override
    public String toString() {
        return "Antibody{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", catalogCode='" + catalogCode + '\'' +
                ", isotype='" + isotype + '\'' +
                ", supplier=" + supplier +
                ", antigen=" + antigen +
                ", host='" + hostOrganism + '\'' +
                ", performanceComments='" + performanceComments + '\'' +
                ", applications=" + applications +
                ", structureLabelingNote='" + structureLabelingNote + '\'' +
                '}';
    }
}
