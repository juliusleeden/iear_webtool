package nl.bioinf.iear.dao.Jdbc;

import nl.bioinf.iear.dao.UserDAO;
import nl.bioinf.iear.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by rHut on 19-4-18.
 * Handles all user related database queries.
 */

@Component
public class UserDataSourceJdbc implements UserDAO{
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UserDataSourceJdbc(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    /**
     * Returns a list of all Users in objects.
     * @return List<User> userModel list
     */
    @Override
    public List<User> getAllUsers() {
        String sql = "SELECT userName, userRole FROM users";
        return jdbcTemplate.query(sql, new UserRowMapper());
    }

    /**
     * Returns a user object by it's username.
     * @return User object
     */
    @Override
    public User getUserByUserName(String username) {
        return namedParameterJdbcTemplate.queryForObject("SELECT userName, aes_decrypt(password, 'iEAR')" +
                        " AS password, userRole FROM users WHERE userName =:username;",
                new MapSqlParameterSource("username", username),
                new UserRowMapper());
    }

    /**
     * returns the password of a given user
     * @param user object
     * @return String userPassword
     */
    @Override
    public String getUserPassword(User user) {
        return namedParameterJdbcTemplate.queryForObject("SELECT AES_DECRYPT(password, 'iEAR') as password FROM users WHERE userName =:username;",
                new MapSqlParameterSource("username", user.getUsername()),
                new PasswordRowMapper());
    }

    /**
     * Inserts a new user into the user database
     * @param user User user
     */
    @Override
    public void insertUser(User user){
        String sql = "INSERT INTO users(userName, password, userRole) VALUES(?, AES_ENCRYPT(?, 'iEAR'), ?);";
        jdbcTemplate.update(sql, user.getUsername(), user.getPassword(), user.getRole());

    }

    /**
     * Deletes a user with a given name
     * @param userName String userName
     */
    @Override
    public void deleteUser(String userName) {
        String sql = "DELETE FROM users WHERE userName = '" + userName + "';";
        jdbcTemplate.update(sql);
    }

    /**
     * Updates a user with a given user
     * @param user Object
     */
    @Override
    public void updateUser(User user) {
        String sql = "UPDATE users SET password = AES_ENCRYPT(?, 'iEAR'), userRole = ? WHERE userName = ?;";
        jdbcTemplate.update(sql, user.getPassword(), user.getRole(), user.getUsername());
    }

    /**
     * Updates a user without updating the password given a user
     * @param user Object
     */
    @Override
    public void updateUserNoPassword(User user) {
        String sql = "UPDATE users SET userRole = ? WHERE userName = ?;";
        jdbcTemplate.update(sql, user.getRole(), user.getUsername());
    }

    private class UserRowMapper implements RowMapper<User> {
        /**
         * Returns a User object filled with the row data
         * @param rs ResultSet
         * @param rowNum int
         * @return userModel usermodel
         * @throws SQLException Exception
         */
        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            //Values that can not be empty
            String userName = rs.getString("userName");
            String role = rs.getString("userRole");
            return new User(userName, role);
        }
    }

    private class PasswordRowMapper implements RowMapper<String> {
        /**
         * Returns a String containing the user password
         * @param rs ResultSet
         * @param rowNum RowNumber
         * @return String password
         * @throws SQLException exception
         */
        @Override
        public String mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getString("password");
        }
    }
}

