package nl.bioinf.iear.control;

import com.google.gson.Gson;
import nl.bioinf.iear.model.*;
import nl.bioinf.iear.backend.DatabaseBackup;
import nl.bioinf.iear.model.Antibody;
import nl.bioinf.iear.model.Suggestion;
import nl.bioinf.iear.model.Supplier;
import nl.bioinf.iear.model.User;
import nl.bioinf.iear.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by jpvanderleeden on 06-6-18.
 * RestController, Controls all information requests that do not have a View.
 */

@org.springframework.web.bind.annotation.RestController

public class RestController {

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Autowired
    private final AntibodyService antibodyService;

    @Autowired
    private final UserService userService;

    @Autowired
    private final SupplierService supplierService;

    @Autowired
    private final SuggestionService suggestionService;

    @Autowired
    private final HistoryService historyService;

    @Autowired
    private final DatabaseBackup databaseBackup;

    public RestController(AntibodyService antibodyService, UserService userService, SupplierService supplierService, SuggestionService suggestionService, HistoryService historyService, DatabaseBackup databaseBackup) {
        this.antibodyService = antibodyService;
        this.userService = userService;
        this.supplierService = supplierService;
        this.suggestionService = suggestionService;
        this.historyService = historyService;
        this.databaseBackup = databaseBackup;
    }

    /**
     * Sends a history message to the database.
     * @param message Message explaining what has happened.
     */
    private void sendUserHistory(String message){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        History history = new History(auth.getName(), message);
        historyService.insertHistoryMessage(history);
    }

    //Antibody mapping
    @RequestMapping(value = "data/antibodies")
    @ResponseBody
    public String listAntibodies() {
        List<Antibody> antibodyList = antibodyService.listAntibodies();
        return new Gson().toJson(antibodyList);
    }

    //Supplier Mapping
    @RequestMapping(value = "data/suppliers")
    @ResponseBody
    public String listSuppliers(){
        List<Supplier> supplierList = supplierService.listSuppliers();
        return new Gson().toJson(supplierList);
    }

    //User Mapping
    @RequestMapping(value = "/data/users")
    @ResponseBody
    public String listUsers(){
        List<User> userList = userService.getAllUsers();
        return new Gson().toJson(userList);
    }

    //History Mapping
    @RequestMapping(value = "/data/history")
    @ResponseBody
    public String listHistories(){
        List<History> historyList = historyService.getAllHistoryMessages();
        return new Gson().toJson(historyList);
    }

    //Suggestion Mapping
    @RequestMapping(value = "/data/suggestions")
    @ResponseBody
    public String listSuggestions() {
        List<Suggestion> suggestionList = suggestionService.getAllSuggestions();
        return new Gson().toJson(suggestionList);
    }

    @RequestMapping(value = "/data/users/insertUser", headers = "Accept=application/json", method = RequestMethod.POST)
    @ResponseBody
    public String insertUserEntry(@RequestBody User user) {
        userService.insertUser(user);
        sendUserHistory("Inserted user: " + user.getUsername());
        return new Gson().toJson("Successfully inserted \n " + "user with user name: " + user.getUsername());
    }

    @RequestMapping(value = "/data/users/deleteUser", headers = "Accept=application/json", method = RequestMethod.POST)
    @ResponseBody
    public String deleteUser(@RequestBody String userName) {
        userService.deleteUser(userName);
        sendUserHistory("Deleted user: " + userName);
        return new Gson().toJson("Successfully deleted \n " + "user with user name: " + userName);
    }

    @RequestMapping(value = "/data/users/editUser", headers = "Accept=application/json", method = RequestMethod.POST)
    @ResponseBody
    public String editUserEntry(@RequestBody User user) {
        // Password is optional when updating a user.
        if (user.getPassword().equals("")) {
            userService.updateUserWithoutPassword(user);
        } else {
            userService.updateUser(user);
        }
        sendUserHistory("Updated user: " + user.getUsername());
        return new Gson().toJson("Successfully updated \n " + "user with user name: " + user.getUsername());
    }

    @RequestMapping(value = "/data/insertAntibody", headers = "Accept=application/json", method = RequestMethod.POST)
    @ResponseBody
    public String insertAntibodyEntry(@RequestBody Antibody antibody) {
        antibodyService.insertAntibody(antibody);
        sendUserHistory("Inserted new antibody: " + antibody.getCatalogCode());
        return new Gson().toJson("Successfully inserted \n " + "Antibody with catalog code: " + antibody.getCatalogCode());
    }

    @RequestMapping(value = "/data/editAntibody", headers = "Accept=application/json", method = RequestMethod.POST)
    @ResponseBody
    public String editAntibodyEntry(@RequestBody Antibody antibody) {
        sendUserHistory("Updated antibody: " + antibody.getCatalogCode());
        antibodyService.updateAntibody(antibody);
        return new Gson().toJson("Successfully updated \n " + "Antibody with catalog code: " + antibody.getCatalogCode());
    }

    @RequestMapping(value = "/data/confirmDelete/{id}", headers = "Accept=application/json", method = RequestMethod.POST)
    @ResponseBody
    public String deleteAntibodyEntry(@PathVariable int id) {
        Antibody antibody = antibodyService.getAntibodyById(id);
        sendUserHistory("Deleted antibody: " + antibody.getCatalogCode());
        antibodyService.deleteAntibody(id);
        return new Gson().toJson(antibody.getCatalogCode() + "has been deleted");
    }

    @RequestMapping(value = "/suggestion/insert", headers = "Accept=application/json", method = RequestMethod.POST)
    @ResponseBody
    public String insertSuggestion(@RequestBody Suggestion suggestion) {
        suggestionService.insertSuggestion(suggestion);
        return new Gson().toJson("Successfully inserted suggestion into the database, its now awaiting review.");
    }

    @RequestMapping(value = "/suggestion/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public void deleteSuggestion(@PathVariable int id) {
        sendUserHistory("Deleted a suggestion");
        suggestionService.deleteSuggestion(id); }

    @RequestMapping(value = "/data/clearHistory", method = RequestMethod.POST)
    @ResponseBody
    public String clearHistory(){
        historyService.clearOldHistoryMessages();
        sendUserHistory("Cleared history log");
        return new Gson().toJson("Cleared messages older than 14 days from log");
    }

    @RequestMapping(value = "/data/newSupplier", headers = "Accept=application/json", method = RequestMethod.POST)
    @ResponseBody
    public String insertSupplier(@RequestBody Supplier supplier){
        supplierService.insertSupplier(supplier);
        sendUserHistory("Inserted Supplier: " + supplier.getName());
        return new Gson().toJson("Successfully inserted \n " + "supplier with name: " + supplier.getName());
    }

    @RequestMapping(value = "/admin/makeBackup", method = RequestMethod.POST)
    @ResponseBody
    public String makeDatabaseBackup() {
        sendUserHistory("Created a backup of the database.");
        return databaseBackup.makeBackup(username, password);
    }
}
