package nl.bioinf.iear.service;

import nl.bioinf.iear.dao.UserDAO;
import nl.bioinf.iear.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rHut on 18-4-18.
 * Service that implements UserDataSourceJdbc.
 */

@Service
public class UserService {
    private final UserDAO userDAO;

    @Autowired
    public UserService(UserDAO userDAO) { this.userDAO = userDAO; }

    public List<User> getAllUsers() {return userDAO.getAllUsers();}

    public String getUserPassword(User user) { return userDAO.getUserPassword(user); }

    public User getUserByUserName(String userName) {return userDAO.getUserByUserName(userName);}

    public void insertUser(User user) { userDAO.insertUser(user); }

    public void deleteUser(String userName) { userDAO.deleteUser(userName); }

    public void updateUser(User user) { userDAO.updateUser(user);}

    public void updateUserWithoutPassword(User user) { userDAO.updateUserNoPassword(user);}

}
