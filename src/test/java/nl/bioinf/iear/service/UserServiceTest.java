package nl.bioinf.iear.service;

import nl.bioinf.iear.model.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest()

public class UserServiceTest {
    private String nameTest = "JanJaap@hotmail.com";
    private String passwordTest = "password";
    private String roleTest = "USER";
    private User testUser = new User(nameTest, passwordTest, roleTest);

    @Autowired
    private UserService userService;

    @Before
    public void setUp() {
        userService.insertUser(testUser);
    }

    @After
    public void tearDown() {
        userService.deleteUser(testUser.getUsername());
    }

    //User list tests
    @Test
    public void userListSunny() {
        List<User> userList = userService.getAllUsers();
        assertNotNull(userList);
    }

    //Insert user tests
    @Test
    public void insertUserSunny() {
        final String expectedName = "JanJaap@hotmail.com";
        final User result = userService.getUserByUserName("JanJaap@hotmail.com");
        assertEquals(expectedName, result.getUsername());
    }

    @Test(expected = DuplicateKeyException.class)
    public void insertDuplicateUser() {
        userService.insertUser(testUser);
    }

    //Get user by username tests
    @Test
    public void getUserByUserNameSunny() {
        User user = userService.getUserByUserName("JanJaap@hotmail.com");
        String expectedUserName = "JanJaap@hotmail.com";
        assertEquals(expectedUserName, user.getUsername());
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void getUserNameNotExisting() {
        userService.getUserByUserName("ThisUserDoesNotExist");
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void getUserByUserNameNull() {
        userService.getUserByUserName(null);
    }

    //Delete user tests
    @Test(expected = EmptyResultDataAccessException.class)
    public void deleteUserSunny() {
        assertNotNull(userService.getUserByUserName(testUser.getUsername()));
        userService.deleteUser(testUser.getUsername());
        userService.getUserByUserName(testUser.getUsername());
    }

    @Test (expected = BadSqlGrammarException.class)
    public void deleteUserNotExisting(){
        userService.deleteUser("Doesn'texist");
    }

    //Update user tests
    @Test
    public void updateUserSunny() {
        User updatedUser = new User("JanJaap@hotmail.com", "password", "ADMIN");
        userService.updateUser(updatedUser);
        User actualUser = userService.getUserByUserName("JanJaap@hotmail.com");
        assertEquals(updatedUser.getRole(), actualUser.getRole());
        userService.deleteUser(updatedUser.getUsername());
    }

    @Test
    public void updateUserBoundary() {
        User updatedUser = new User(null, null, null);
        userService.updateUser(updatedUser);
    }

}