/**
 * Name: Admin
 * Version: 0.3.1.8.6.13
 * Author: RgHut
 * Use: handles admin backup and log controls
 */

$(document).ready(function () {

    //Starts the back up button modal.
    $('#backupButton').click(function () {
        $('#backupModal').modal();
    });

    //Confirm the back up. Calls makeBackup.
    $('#backupYes').click(function () {
        var modal = $('#backupModal');
        var href = "/admin/makeBackup";
        $.ajax({
            url: href,
            type: 'POST',
            success: function (result) {
                var modal = $('#messageModal');
                modal.find('.modal-body').text(result);
                modal.modal();
            },
            error: function (data, status, er) {
                alert("error: " + data + " status: " + status + " er: " + er)
            }
        });
        modal.modal('hide');
    });

    //Starts the clear log modal.
    $('#clear_log').click(function () {
        $('#clearHistoryModal').modal();
    });

    //Confirm the clear log. Calls the clearHistory.
    $('#logClearYes').click(function () {
        var modal = $('#clearHistoryModal');
        var href = "/data/clearHistory";
        $.ajax({
            url: href,
            type: 'POST',
            success: function (result) {
                var message = $('#messageModal');
                message.find('.modal-body').text(result);
                message.modal();
            },
            error: function (data, status, er) {
                alert("error: " + data + " status: " + status + " er: " + er)
            }
        });
        modal.modal('hide');
    });

});