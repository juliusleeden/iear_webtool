package nl.bioinf.iear.backend;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by RHut on 12-6-18.
 * Receives a Shell command and runs it.
 * some shell symbols are omitted and will not run. (for example "<", ">")
 */

@Service
public class ExecuteShellCommand {

    public static void main(String command) {

        ExecuteShellCommand obj = new ExecuteShellCommand();

        String output = obj.executeCommand(command);

        System.out.println(output);

    }

    private String executeCommand(String command) {
        StringBuilder output = new StringBuilder();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line;
            while ((line = reader.readLine())!= null) {
                output.append(line).append("\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();
    }
}