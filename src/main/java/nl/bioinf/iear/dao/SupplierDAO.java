package nl.bioinf.iear.dao;

import nl.bioinf.iear.model.Supplier;

import java.util.List;

/**
 * Created by jpvanderleeden on 9-5-18.
 * Interface for SupplierDataSourceJdbc.
 */

public interface SupplierDAO {

    List<Supplier> listSuppliers();

    Supplier getSupplierById(int supplierId);

    int getSupplierIdByName(String supplierName);

    void insertSupplier(Supplier supplier);
}
