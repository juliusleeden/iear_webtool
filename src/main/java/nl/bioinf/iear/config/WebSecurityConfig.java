package nl.bioinf.iear.config;

import nl.bioinf.iear.model.User;
import nl.bioinf.iear.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.List;

/**
 * Created by RHut on 15-3-18.
 * Configuration for web security.
 */

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserService userService;

    @Autowired
    public WebSecurityConfig(UserService userService) {
        this.userService = userService;
    }

    /**
     * This config makes home, login, available to everyone, as well as all styling, images, javascript and supplier/antibody data
     * Restricts access to /admin page, user/history data and the ability to create backups to anyone but the admins
     * @param httpSecurity httpSecurity object
     * @throws Exception exception
     */
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "/error", "/home", "/*/home", "/images/**", "/css/**", "/js/**",
                        "/data/antibodies", "/webjars/**", "/suggestion/insert")
                .permitAll()
                .antMatchers("/data/users", "/data/history", "/user", "/admin", "/admin/backup").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/home")
                .permitAll()
                .and()
                /*The mapping /logout will automatically log the user out, without any html page. Will redirect to /login?logout*/
                .logout()
                .permitAll();

    }

    @Bean
    public UserDetailsService userDetailsService() {
        List<User> userList = userService.getAllUsers();
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        for (User user : userList) {
            String password = userService.getUserPassword(user);
            if (user.getRole().equals("ADMIN")){
                manager.createUser(org.springframework.security.core.userdetails.User.withUsername(user.getUsername()).password(password).roles("USER", user.getRole()).build());
            }
            else {
            manager.createUser(org.springframework.security.core.userdetails.User.withUsername(user.getUsername()).password(password).roles(user.getRole()).build());
            }
        }
        return manager;
    }
}