/**
 * Name: TicketControl
 * Version: 0.1.0.1.8.6.06
 * Author: Rghut
 * Use: showing suggestion tickets to admins and removing them
 */

$(document).ready(function () {
    const url = '/data/suggestions';

    //Holds all the cards.
    var cardList = $('#suggestionCards');
    cardList.empty();

    //Handles the JSON request, obtaining all the suggestions and turning them into cards.
    $.getJSON(url, function (data) {
        $.each(data, function (key, entry) {
            cardList.append($('<li id="' + entry.id + '">\n' +
                '<div class="card">\n' +
                '<div class="card-body">\n' +
                '<h6 class="card-title">' + entry.suggestionType + '</h6>\n' +
                '<h6 class="card-subtitle mb-2 text-muted">' + entry.timeStamp + '</h6>\n' +
                '<p class="card-text">\n' + entry.suggestionText +
                '</p>\n' +
                '<button type="button" id="suggestionCardRemove" class="btn btn-primary" value="' + entry.id + '">Remove Card</button>\n' +
                '</div>\n' +
                '</div>\n' +
                '<br /></li>'));
        })
    });

    //Allows for the deletion of a card. Sends the ID to /suggestion/delete/.
    cardList.on("click", "button#suggestionCardRemove.btn.btn-primary", function () {
        var id = this.value;
        var href = "/suggestion/delete/" + id;
        $.ajax({
            type: "GET",
            contentType: false,
            url: href,
            success: function (result) {
                console.log("SUCCES", result);
                var item = document.getElementById(id);
                cardList[0].removeChild(item);
            },
            error: function (e) {
                console.log("ERROR:", e);
            }
        });
    });
});