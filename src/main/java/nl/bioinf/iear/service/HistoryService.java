package nl.bioinf.iear.service;

import nl.bioinf.iear.dao.HistoryDAO;
import nl.bioinf.iear.model.History;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jpvanderleeden on 05-6-18.
 * Service that implements AntibodyDataSourceJdbc.
 */
@Service

public class HistoryService {
    private final HistoryDAO historyDAO;

    @Autowired
    public HistoryService(HistoryDAO historyDAO) { this.historyDAO = historyDAO; }

    public History insertHistoryMessage(History history) { return historyDAO.insertHistoryMessage(history);}

    public List<History> getAllHistoryMessages() { return historyDAO.getAllHistoryMessages();}

    public boolean clearOldHistoryMessages() { return historyDAO.clearOldHistoryMessages(); }

}

