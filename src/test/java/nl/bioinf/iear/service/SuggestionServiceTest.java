package nl.bioinf.iear.service;

import nl.bioinf.iear.model.Suggestion;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SuggestionServiceTest {

    @Mock
    private
    SuggestionService suggestionService;

    private List<Suggestion> suggestions = new ArrayList<>();
    private List<Suggestion> emptySuggestions = new ArrayList<>();
    private List<Suggestion> isotypeSuggestions = new ArrayList<>();

    @Before
    public void setUp(){
        suggestions.add(new Suggestion("Account Request", "Suggestion1"));
        suggestions.add(new Suggestion("Antibody Database", "Suggestion2"));
        suggestions.add(new Suggestion("Development", "Suggestion3"));

        isotypeSuggestions.add(new Suggestion("Development", "Suggestion3"));
    }

    @Test
    public void getAllSuggestionsSunny(){
        when(suggestionService.getAllSuggestions()).thenReturn(suggestions);
        assertEquals(suggestions, suggestionService.getAllSuggestions());
    }

    @Test
    public void getAllSuggestionsEmpty(){
        when(suggestionService.getAllSuggestions()).thenReturn(new ArrayList<>());
        assertEquals(emptySuggestions, suggestionService.getAllSuggestions());
    }

    @Test
    public void getAllSuggestionsByIsotypeSunny(){
        when(suggestionService.getAllSuggestionsByType("Development")).thenReturn(isotypeSuggestions);
        assertEquals(isotypeSuggestions, suggestionService.getAllSuggestionsByType("Development"));
    }

    @Test
    public void getAllSuggestionsByIsotypeEmpty(){
        when(suggestionService.getAllSuggestionsByType("Account Request")).thenReturn(new ArrayList<>());
        assertEquals(emptySuggestions, suggestionService.getAllSuggestionsByType("Account Request"));
    }

    @Test
    public void getSuggestionByIdSunny(){
        when(suggestionService.getSuggestionById(1)).thenReturn(suggestions.get(1));
        assertEquals(suggestions.get(1), suggestionService.getSuggestionById(1));
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void getSuggestionByIdBoundary(){
        when(suggestionService.getSuggestionById(8)).thenReturn(suggestions.get(8));
    }

    @Test
    public void insertSuggestionSunny(){
        Suggestion suggestion = new Suggestion("Development", "New development suggestion");
        when(suggestionService.insertSuggestion(suggestion)).thenReturn(suggestion);
        assertEquals(suggestion, suggestionService.insertSuggestion(suggestion));
    }

    @Test
    public void insertSuggestionBoundary(){
        Suggestion suggestion = new Suggestion("", "");
        when(suggestionService.insertSuggestion(suggestion)).thenReturn(suggestion);
        assertEquals(suggestion, suggestionService.insertSuggestion(suggestion));
    }

    @Test
    public void deleteSuggestionSunny(){
        when(suggestionService.deleteSuggestion(1)).thenReturn(true);
        assertEquals(suggestionService.deleteSuggestion(1), true);
    }

    @Test
    public void deleteSuggestionBoundary(){
        when(suggestionService.deleteSuggestion(100)).thenReturn(false);
        assertEquals(suggestionService.deleteSuggestion(100), false);
    }
}