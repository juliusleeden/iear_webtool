
package nl.bioinf.iear.dao.Jdbc;

import nl.bioinf.iear.dao.AntibodyDAO;
import nl.bioinf.iear.model.Antibody;
import nl.bioinf.iear.model.Antigen;
import nl.bioinf.iear.model.Supplier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by jpvanderleeden on 15-3-18.
 * Handles all antibody related database queries.
 */

@Component
public class AntibodyDataSourceJdbc implements AntibodyDAO {
    @Value("${UNAVAILABLE_SYMBOL}")
    private String UNAVAILABLE_SYMBOL;

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public AntibodyDataSourceJdbc(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    /**
     * Returns a list of all antibodies in objects.
     * @return List<Antibody> antibody list
     */
    @Override
    public List<Antibody> getAllAntibodies() {
        String sql = "SELECT * FROM antibodies an " +
                "JOIN antigens a " +
                "ON an.antibody_id = a.antibody_id " +
                "JOIN suppliers s ON" +
                " an.supplier_id = s.supplier_id ";
        return jdbcTemplate.query(sql, new AntibodyRowMapper());
    }

    /**
     * Returns a list filled with antibody objects that have a specific isotype.
     * @param isotype The isotype
     * @return List<Antibody> Antibody list
     */
    @Override
    public List<Antibody> getAllAntibodiesByIsotype(String isotype) {
        String sql = "SELECT * FROM antibodies an " +
                "join antigens a " +
                "ON an.antibody_id = a.antibody_id " +
                "JOIN suppliers s ON" +
                " an.supplier_id = s.supplier_id " +
                "WHERE an.isotype = '" + isotype + "'";
        return jdbcTemplate.query(sql, new AntibodyRowMapper());
    }

    /**
     * Returns a antibody object that matches the idPattern.
     * @param idPattern int idPattern
     * @return Antibody object
     */
    @Override
    public Antibody getAntibodyById(int idPattern) {
        String sql = "SELECT * FROM antibodies an " +
                "JOIN antigens a " +
                "ON an.antibody_id = a.antibody_id " +
                "JOIN suppliers s " +
                "ON an.supplier_id = s.supplier_id " +
                "WHERE an.antibody_id =:id;";
        return namedParameterJdbcTemplate.queryForObject(sql,
                new MapSqlParameterSource("id", idPattern),
                new AntibodyRowMapper());
    }

    /**
     * Returns a antibody object that matches the catalogCode.
     * @param catalogCode String catalog code
     * @return Antibody object
     */
    @Override
    public Antibody getAntibodyByCatalogCode(String catalogCode) {
        String sql = "SELECT * FROM antibodies an " +
                "JOIN antigens a " +
                "ON an.antibody_id = a.antibody_id " +
                "JOIN suppliers s " +
                "ON an.supplier_id = s.supplier_id " +
                "WHERE catalog_number =:code;";
        return namedParameterJdbcTemplate.queryForObject(sql,
                new MapSqlParameterSource("code", catalogCode),
                new AntibodyRowMapper());
    }

    /**
     * inserts a new antibody into the database
     * @param antibody Object
     */
    @Override
    public void insertAntibody(Antibody antibody) {
        String antibodySql = "INSERT INTO antibodies (" +
                "antibody_name, " +
                "catalog_number," +
                " isotype, " +
                "supplier_id, " +
                "antibody_host, " +
                "applications, " +
                "structureLabelingNote, " +
                "works) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";

        jdbcTemplate.update(antibodySql,
                antibody.getName(),
                antibody.getCatalogCode(),
                antibody.getIsotype(),
                antibody.getSupplier().getId(),
                antibody.getHostOrganism(),
                antibody.getApplications(),
                antibody.getStructureLabelingNote(),
                antibody.getPerformanceComments());

        String antigenSql = "INSERT INTO antigens (" +
                "antigen, " +
                "antigen_details, " +
                "gene, " +
                "antibody_id) " +
                "VALUES (?, ?, ?, LAST_INSERT_ID());";

        jdbcTemplate.update(antigenSql,
                antibody.getAntigen().getName(),
                antibody.getAntigen().getDescription(),
                antibody.getAntigen().getGeneSymbol());
    }

    /**
     * Deletes an existing antibody that matches the antibodyId
     * @param antibodyId int antibodyid
     */
    @Override
    public void deleteAntibody(int antibodyId) {
        String antigenSql = "DELETE FROM antigens WHERE antibody_id = ?;";
        jdbcTemplate.update(antigenSql, antibodyId);
        String antibodySql = "DELETE FROM antibodies WHERE antibody_id = ?;";
        jdbcTemplate.update(antibodySql, antibodyId);
    }

    /**
     * Updates an exsiting antibody with the new Antibody
     * @param antibody Antibody object
     */
    @Override
    public void updateAntibody(Antibody antibody) {
        String sqlAntibody = "UPDATE antibodies SET " +
                "antibody_name = '" + antibody.getName() + "', " +
                "catalog_number = '" + antibody.getCatalogCode() + "', " +
                "isotype = '" + antibody.getIsotype() + "', " +
                "supplier_id = " + antibody.getSupplier().getId() + ", " +
                "antibody_host = '" + antibody.getHostOrganism() + "', " +
                "applications = '" + antibody.getApplications() + "', " +
                "structureLabelingNote = '" + antibody.getStructureLabelingNote() +
                "', works = '" + antibody.getPerformanceComments() +
                "' WHERE antibody_id = " + antibody.getId() + ";";
        jdbcTemplate.update(sqlAntibody);

        String sqlAntigen = "UPDATE antigens SET antigen = ?, antigen_details = ?, gene = ? WHERE antibody_id = ?;";
        jdbcTemplate.update(sqlAntigen, antibody.getAntigen().getName(), antibody.getAntigen().getDescription(),
                antibody.getAntigen().getGeneSymbol(), antibody.getId());
    }


    private class AntibodyRowMapper implements RowMapper<Antibody> {
//        private static final String UNAVAILABLE_SYMBOL = "-";

        /**
         * Returns an Antibody object filled with the row data
         * @param rs Resultset
         * @param rowNum int
         * @return antibody
         * @throws SQLException Exception
         */
        @Override
        public Antibody mapRow(ResultSet rs, int rowNum) throws SQLException {
            //Values that can not be empty
            int antibodyId = rs.getInt("antibody_id");
            String catalogCode = rs.getString("catalog_number");
            String isotype = rs.getString("isotype");
            String hostOrganism = rs.getString("antibody_host");

            //Become UNAVAILABLE_SYMBOL = "-" if empty.
            String antibodyName = rs.getString("antibody_name");
            antibodyName = antibodyName == null ? UNAVAILABLE_SYMBOL : antibodyName;

            String structureLabelingNote = rs.getString("structureLabelingNote");
            structureLabelingNote = structureLabelingNote == null ? UNAVAILABLE_SYMBOL : structureLabelingNote;

            String performanceComments = rs.getString("works");
            performanceComments = performanceComments == null ? UNAVAILABLE_SYMBOL : performanceComments;

            String applications = rs.getString("applications");
            applications = applications == null ? UNAVAILABLE_SYMBOL : applications;

            int supplierId = rs.getInt("supplier_id");
            String supplierName = rs.getString("supplier_name");
            supplierName = supplierName == null ? UNAVAILABLE_SYMBOL : supplierName;

            String supplierContactInformation = rs.getString("link");
            supplierContactInformation = supplierContactInformation == null ? UNAVAILABLE_SYMBOL : supplierContactInformation;

            int antigenId = rs.getInt("antigen_id");
            String antigenName = rs.getString("antigen");
            antigenName = antigenName == null ? UNAVAILABLE_SYMBOL : antigenName;

            String antigenDescription = rs.getString("antigen_details");
            antigenDescription = antigenDescription == null ? UNAVAILABLE_SYMBOL : antigenDescription;

            String geneSymbol = rs.getString("gene");
            geneSymbol = geneSymbol == null ? UNAVAILABLE_SYMBOL : geneSymbol;

            Antigen antigen = new Antigen(antigenName, antigenDescription, geneSymbol, antigenId);

            Supplier supplier = new Supplier(supplierId);
            supplier.setContactLink(supplierContactInformation);
            supplier.setName(supplierName);

            Antibody antibody = new Antibody(catalogCode, isotype, supplier, antigen, hostOrganism, antibodyId);
            antibody.setName(antibodyName);
            antibody.setStructureLabelingNote(structureLabelingNote);
            antibody.setPerformanceComments(performanceComments);
            antibody.setApplications(applications);

            return antibody;
        }
    }
}
