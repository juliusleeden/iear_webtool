package nl.bioinf.iear.model;

/**
 * Created by rHut on 05-6-18.
 * POJO for Suggestion objects
 */

public class Suggestion {
    private int id;
    private String suggestionType;
    private String suggestionText;
    private String timeStamp;

    public Suggestion(String suggestionType, String suggestionText) {
        this.suggestionType = suggestionType;
        this.suggestionText = suggestionText;
    }

    public Suggestion(int id, String suggestionType, String suggestionText, String timeStamp){
        this.id = id;
        this.suggestionType = suggestionType;
        this.suggestionText = suggestionText;
        this.timeStamp = timeStamp;
    }

    public Suggestion() { }

    public String getSuggestionType() {
        return suggestionType;
    }

    public String getSuggestionText() {
        return suggestionText;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Suggestion{" +
                "id=" + id +
                ", suggestionType='" + suggestionType + '\'' +
                ", suggestionText='" + suggestionText + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                '}';
    }
}
