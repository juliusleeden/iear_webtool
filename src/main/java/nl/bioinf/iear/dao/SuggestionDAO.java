package nl.bioinf.iear.dao;

import nl.bioinf.iear.model.Suggestion;

import java.util.List;

/**
 * Created by rHut on 05-6-18.
 * Interface for the SuggestionDataSourceJdbc.
 */

public interface SuggestionDAO {

    List<Suggestion> getAllSuggestions();

    List<Suggestion> getAllSuggestionsByType(String suggestionType);

    Suggestion getSuggestionById(int id);

    Suggestion insertSuggestion(Suggestion suggestion);

    boolean deleteSuggestion(int id);
}
