package nl.bioinf.iear.dao;

import nl.bioinf.iear.model.Antibody;

import java.util.List;

/**
 * Created by jpvanderleeden on 18-4-18.
 * Interface for AntibodyDataSourceJdbc.
 */

public interface AntibodyDAO {

    List<Antibody> getAllAntibodies();

    List<Antibody> getAllAntibodiesByIsotype(String isotype);

    Antibody getAntibodyById(int idPattern);

    Antibody getAntibodyByCatalogCode(String catalogCode);

    void insertAntibody(Antibody antibody) ;

    void deleteAntibody(int antibodyId);

    void updateAntibody(Antibody antibody);
}
