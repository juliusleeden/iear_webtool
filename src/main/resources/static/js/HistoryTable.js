/**
 * Name: AntibodyTable
 * Version: 0.5.8.1.8.5.31
 * Author: JvdLeeden
 * Use: loading DataTable for history
 */

$(document).ready(function () {
    var aHistoryTable = $('#history_table');
    aHistoryTable.DataTable({
        ajax: "/data/history",
        ajaxDataProp: "",
        "scrollY": "500px",
        "scrollCollapse": true,
        "paging": true,
        autofill: true,
        columns: [
            {data: "username"},
            {data: "message"},
            {data: "timeStamp"}
        ]
    });
});