package nl.bioinf.iear.dao;

import nl.bioinf.iear.model.User;

import java.util.List;

/**
 * Created by rHut on 18-4-18.
 * Interface for the UserDataSourceJdbc.
 */

public interface UserDAO {
    List<User> getAllUsers();

    User getUserByUserName(String userName);

    String getUserPassword(User user);

    void insertUser(User user);

    void deleteUser(String userName);

    void updateUser(User user);

    void updateUserNoPassword(User user);
}
