package nl.bioinf.iear.dao.Jdbc;

import nl.bioinf.iear.dao.SupplierDAO;
import nl.bioinf.iear.model.Supplier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by jpvanderleeden on 9-5-18.
 * Handles interactions with the database for supplier data.
 */

@Component
public class SupplierDataSourceJdbc implements SupplierDAO {
    @Value("${UNAVAILABLE_SYMBOL}")
    private String UNAVAILABLE_SYMBOL;

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public SupplierDataSourceJdbc(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    /**
     * Returns a list of all the suppliers
     * @return List<Supplier> list
     */
    @Override
    public List<Supplier> listSuppliers() {
        String sql = "SELECT * FROM suppliers ORDER BY supplier_name;";
        return jdbcTemplate.query(sql, new SupplierRowMapper());
    }

    /**
     * Returns a supplier with a given id
     * @param supplierId int supplierId
     * @return Supplier object
     */
    @Override
    public Supplier getSupplierById(int supplierId) {
        String sql = "SELECT * FROM suppliers WHERE supplier_id =:id;";
        return namedParameterJdbcTemplate.queryForObject(sql,
                new MapSqlParameterSource("id", supplierId),
                new SupplierRowMapper());
    }

    /**
     * Returns the id of a supplier with a certain name
     * @param supplierName String supplier name
     * @return int supplierId
     */
    @Override
    public int getSupplierIdByName(String supplierName) {
        String sql = "SELECT supplier_id FROM suppliers WHERE supplier_name = ?;";
        return jdbcTemplate.queryForObject(
                sql, new Object[] { supplierName }, int.class);
    }

    /**
     * inserts a new supplier into the database
     * @param supplier object
     */
    @Override
    public void insertSupplier(Supplier supplier) {
        String sql = "INSERT INTO suppliers(supplier_name, link) VALUES(?, ?);";
        jdbcTemplate.update(sql, supplier.getName(), supplier.getContactLink());
    }

    private class SupplierRowMapper implements RowMapper<Supplier> {
        /**
         * Returns a supplier object filled with the row data
         * @param rs Resultset
         * @param rowNum RowNumber
         * @return Supplier object
         * @throws SQLException exception
         */
        @Override
        public Supplier mapRow(ResultSet rs, int rowNum) throws SQLException{

            String supplierLink = rs.getString("link");
            supplierLink = supplierLink == null ? UNAVAILABLE_SYMBOL : supplierLink;

            Supplier supplier = new Supplier(rs.getInt("supplier_id"));
            supplier.setName(rs.getString("supplier_name"));
            supplier.setContactLink(supplierLink);
            return supplier;
        }
    }
}
