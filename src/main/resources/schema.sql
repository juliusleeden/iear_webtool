drop table if exists antigens;
drop table if exists antibodies;
drop table if exists suppliers;
drop table if exists users;
drop table if exists suggestions;
drop table if exists history;

create table suppliers
( supplier_id INT auto_increment NOT NULL,
  supplier_name VARCHAR(100) NOT NULL,
  link VARCHAR(100),
  PRIMARY KEY (supplier_id)
);

create table antibodies
( antibody_id INT auto_increment NOT NULL,
  antibody_name VARCHAR(30),
  catalog_number VARCHAR(100),
  isotype VARCHAR(50) NOT NULL,
  supplier_id INT NOT NULL,
  antibody_host VARCHAR(30),
  applications VARCHAR(100),
  structureLabelingNote text,
  works VARCHAR(200),
  PRIMARY KEY (antibody_id),
    FOREIGN KEY (supplier_id)
        references suppliers(supplier_id)
);

create table antigens
( antigen_id INT auto_increment NOT NULL,
  antigen VARCHAR(100) NOT NULL,
  antigen_details TEXT,
  gene VARCHAR(40) NOT NULL,
  antibody_id INT NOT NULL,
  PRIMARY KEY (antigen_id),
    FOREIGN KEY (antibody_id)
        references antibodies(antibody_id)
);

create table users
( userName VARCHAR(50) UNIQUE NOT NULL,
  password VARBINARY(200) NOT NULL,
  userRole VARCHAR(20) NOT NULL,
  PRIMARY KEY (userName)
);

create table suggestions
( suggestion_id INT auto_increment NOT NULL,
  suggestion_type VARCHAR(25) NOT NULL,
  suggestion_text TEXT,
  ts TIMESTAMP,
  PRIMARY KEY (suggestion_id)
);

CREATE TABLE history
( history_id INT AUTO_INCREMENT NOT NULL,
  user_name VARCHAR(50) NOT NULL,
  message    VARCHAR(100) NOT NULL,
  ts TIMESTAMP,
  PRIMARY KEY (history_id)
);

# CREATE EVENT delete_suggestion_event
#   ON SCHEDULE AT CURRENT_TIMESTAMP + INTERVAL 1 DAY
#   ON COMPLETION PRESERVE
#   DO
#     DELETE FROM suggestions WHERE ts < (CURDATE() - INTERVAL 30 DAY);


ALTER TABLE antigens CONVERT TO CHARACTER SET utf8;
















