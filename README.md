#iEAR Webtool

The inner-ear antibody resource (iEAR) web tool is a web application that accesses an inner-ear antibody database.
iEAR aims to make searching for and buying antibodies for a specific experiment easier for the researcher.
By having a quick and intuitive way to show the information of all the antibodies in the database,
iEAR prevents the purchase of wrong antibodies. A researcher can search for the location targeted by the desired antibody and which antibodies can be used for a specific application.
When the researcher has found an antibody, they can expand the row in the table to view additional information about that antibody.
If a researcher has found an antibody missing from the database, has discoverd an error in an antibody, or has any other suggestions they can fill out a suggestion form.
These suggestions can be viewed by any logged in user.

A research staff member can be given an user account allowing them to log in.
A logged in user gets the ability to add new antibodies to the database, change the information of antibodies in the database, or remove antibodies from the database.

Some users can be given admin privileges. Administrators can access the admin page where they can: 
make new users or change the information of other users, including giving them admin privileges;
add new suppliers to the database; view a log of all the changes made and by which user; clear this log of old changes; and create a backup of the current database.



##Getting started

To use this project, fork this repository to your own Bitbucket and local machine. Load the project in your preferred IDE and make sure that
the database information is correct in the application.properties file. 

* spring.datasource.url=jdbc:mysql:[location of the iEAR Database]
* spring.datasource.username= [username]
* spring.datasource.password= [password]
* spring.datasource.initialize=true
* BACKUP_DIRECTORY = [backup directory]

After this run the application file. Spring boot will take care of the initiation and after a short moment the tool will become available on: http://localhost:8080/
After the first run change spring.datasource.initialize from true to false.
This will prevent the database from reloading on every start.

##Prerequisites

* mySQL server
* mySQL database with the name iEAR
* tomcat

##Usage
A user arriving on the web page will be met by a loaded table containing all the currently available antibody data, an image of the inner ear and a small login screen. 
![alt text](src/main/resources/static/images/homepage_guest.png)

The user can shift through the table using the search bar at the top or use the page buttons at the bottom of the table allowing them to browse the data and use the drop down menu to specify how many antibodies are shown on each page. Clicking on the different field names at the top of the table will sort the data ascending or descending. If a user has located an antibody of interest and wants to know additional information, they can click on the small logo on the right side of the table. Which will open the row and reveal  extra information about the specific antibody. 
![alt text](src/main/resources/static/images/tableOpen.png)

An anonymous user can affect the database by sending a suggestion. They can click on options this will reveal a button "send suggestion". The user is then shown a modal with the suggestion form.
![alt text](src/main/resources/static/images/suggestion.png)

Researchers that own an account can use their username and password to log in to the iEAR webtool. Logged in users gain access to more features such as: editing existing antibody entries in order to correct falsehoods, deleting existing antibodies when these have become obsolete and adding completely new entries to the database.

![alt text](src/main/resources/static/images/insertAntibody.png)

An administrator can access the admin page. On this page three different tabs are available:
"Users" the default tab which shows the user table.
![alt text](src/main/resources/static/images/userTable.png)
The Administrator can add a user by clicking on options and then on "Add new User". or they can change the information of a user by clicking on the wrench in the table.
![alt text](src/main/resources/static/images/newUser.png)

"Log" this tab shows the message log of all the changes made by users and the time these changes were made.
The Administrator can clear the log messages that are older than 14 days by clicking on options and then on "clear log files".
![alt text](src/main/resources/static/images/messageLog.png)

"Backup" this tab has a button. clicking on this button will reveal a modal asking the admin if they want to back up the database. if the administrator clicks on "yes" a backup will be made of the current database this backup file will have a timestamp of the day it was made and will be stored in the backup directory.
![alt text](src/main/resources/static/images/backup.png)

An administrator can add a new supplier by clicking on options and then "Add new Supplier", and filling out the shown form.

![alt text](src/main/resources/static/images/newSupplier.png)



##Authors

* Julius vd Leeden @ j.p.van.der.leeden@st.hanze.nl [BitBucket](https://bitbucket.org/juliusleeden/)
* Roland Hut @ r.g.hut@st.hanze.nl - developer

