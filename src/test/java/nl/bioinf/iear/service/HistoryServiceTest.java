package nl.bioinf.iear.service;

import nl.bioinf.iear.model.History;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HistoryServiceTest {

    @Mock
    private
    HistoryService historyService;

    @Autowired
    private SuggestionService suggestionService;

    @Test
    public void getAllHistoryMessagesSunny(){
        List<History> historyList = new ArrayList<>();
        historyList.add(new History("Test", "Testmessage"));
        when(historyService.getAllHistoryMessages()).thenReturn(historyList);
        assertEquals(historyList, historyService.getAllHistoryMessages());
    }

    @Test
    public void getAllHistoryMessagesEmpty(){
        List<History> historyList = new ArrayList<>();
        when(historyService.getAllHistoryMessages()).thenReturn(historyList);
        assert historyService.getAllHistoryMessages().isEmpty();
    }

    @Test
    public void insertHistoryMessageSunny(){
        History history = new History("Test", "test message");
        when(historyService.insertHistoryMessage(history)).thenReturn(history);
        assertEquals(history, historyService.insertHistoryMessage(history));
    }

    @Test
    public void insertHistoryMessageNull(){
        when(historyService.insertHistoryMessage(null)).thenReturn(null);
        assertNull(historyService.insertHistoryMessage(null));
    }

    @Test
    public void clearOldHistoryMessages(){
        when(historyService.clearOldHistoryMessages()).thenReturn(true);
        assert historyService.clearOldHistoryMessages();
    }

}