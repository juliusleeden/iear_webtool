package nl.bioinf.iear.service;

import nl.bioinf.iear.model.Suggestion;
import nl.bioinf.iear.model.Supplier;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class SupplierServiceTest {
    private Supplier testSupplier = new Supplier(1);

    @Autowired
    private SupplierService supplierService;

    @Test
    public void listSuppliersSunny() {
        List<Supplier> suppliersList = supplierService.listSuppliers();
        assertNotNull(suppliersList);
    }

    @Test
    public void getSupplierByIdSunny() {
        Supplier supplier = supplierService.getSupplierById(1);
        String expectedName = "SCBT";
        assertEquals(expectedName, supplier.getName());
    }

    @Test (expected = EmptyResultDataAccessException.class)
    public void getSupplierByIdBoundary(){
        Supplier result = supplierService.getSupplierById(1000000000);
    }

    @Test
    public void getSupplierIdByNameSunny() {
        int result = supplierService.getSupplierIdByName("SCBT");
        int expected = 1;
        assertEquals(expected, result);
    }

    @Test (expected = EmptyResultDataAccessException.class)
    public void getSupplierIdByNameBoundary() {
        int result = supplierService.getSupplierIdByName("This doesn't exist");
    }

}
