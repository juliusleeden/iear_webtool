/**
 * Name: SupplierModalControl
 * Version: 0.1.1.8.6.13
 * Author: Rghut
 * Use: controls adding new suppliers to the database
 */

$(document).ready(function () {

    //Starts the new supplier modal.
    $('#new_supplier').click(function () {
        $('#supplierModal').modal();
    });

    //Confirms and sends the new supplier to /data/newSupplier.
    $('#supplierConfirm').click(function () {
        var supplierModal = $('#supplierModal');

        //The new supplier data.
        var supplierName = supplierModal.find('[name="supplierName"]').val();
        var supplierLink = supplierModal.find('[name="supplierLink"]').val();

        if (supplierName !== "") {
            // Save the data in json format.
            var data = {
                name: supplierName,
                contactLink: supplierLink
            };

            //Handy log to show the data before sending it.
            //console.log("data before send ", data);

            //Handles the ajax request.
            var href = "/data/newSupplier";
            $.ajax({
                url: href,
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(data),
                success: function (result) {
                    var modal = $('#messageModal');
                    modal.find('.modal-body').text(result);
                    modal.modal();
                },
                error: function (data, status, er) {
                    console.log("error", data, status, er)
                }
            });
            supplierModal.modal('hide');

            //If statements allowing for a dynamic view. It shows which parts of the modal or not filled in correctly.
        } else {
            supplierModal.find($('#addSupplierName')).text("Supplier Name cant be empty!");
        }
        if (supplierName !== "") {
            supplierModal.find($('#addSupplierName')).text("");
        }
    });

    //Empties the modal values on closing.
    $('#supplierModal').on('hidden.bs.modal', function () {
        var modal = $('#supplierModal');
        modal.find('[name="supplierName"]').val("");
        modal.find('[name="supplierLink"]').val("");
        modal.find($('#addSupplierName')).text("")
    });
});