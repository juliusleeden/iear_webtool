/**
 * Name: AntibodyTable
 * Version: 1.0.1.8.6.13
 * Author: JvdLeeden, Rghut
 * Use: loading DataTable for Antibodies + modal functioning
 */

//Loads the table data and allows for cross script usage.
var Alltables = function () {
    var aTable = $('#antibody_Table');
    var table = aTable.DataTable({
        ajax: "/data/antibodies",
        ajaxDataProp: "",
        "scrollY": "500px",
        "scrollCollapse": true,
        "paging": true,
        autofill: true,
        "columns": tableColumns
    });
    var tableBody = aTable.find('tbody');
    return {
        table: table,
        tableBody: tableBody
    }
}();

$(document).ready(function () {
        var table = Alltables.table;
        var tableBody = Alltables.tableBody;

        //Redraws the table.
        document.getElementById('table_reload').onclick = function () {
            table.search('').columns().search('').draw();
            table.ajax.reload();
        };

        // Add event listener for opening and closing details
        tableBody.on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });
    }
);

