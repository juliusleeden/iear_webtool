package nl.bioinf.iear.model;

/**
 * Created by jpvanderleeden on 07-06-18.
 * POJO for History objects
 */

public class History {
    private int id;
    private String username;
    private String message;
    private String timeStamp;

    public History(int id, String username, String message, String timeStamp) {
        this.id = id;
        this.username = username;
        this.message = message;
        this.timeStamp = timeStamp;
    }

    public History(String username, String message) {
        this.username = username;
        this.message = message;
    }

    public History() { }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", message='" + message + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                '}';
    }
}
