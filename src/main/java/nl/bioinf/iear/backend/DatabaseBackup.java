package nl.bioinf.iear.backend;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by RHut on 12-6-18.
 * makes the string commands for backing up the database and loading a backup.
 * sends this string command to ExecuteShellCommand
 */

@Service
public class DatabaseBackup {

    private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");

    @Value("${BACKUP_DIRECTORY}")
    private String backupDirectory;

    public String makeBackup(String username, String password){
        Date dateObject = new Date();
        String backupFile = backupDirectory + "iEarDatabaseBackup" + dateFormat.format(dateObject) + ".sql";
        String command1 = "touch " + backupFile;
        String command2 = "mysqldump -u " + username + " -p" + password + " iEAR --result-file=" + backupFile;
        ExecuteShellCommand.main(command1);
        ExecuteShellCommand.main(command2);
        return "created backup (" + backupFile + ")";
    }

}
