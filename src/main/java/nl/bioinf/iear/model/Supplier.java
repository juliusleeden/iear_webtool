package nl.bioinf.iear.model;

/**
 * Created by jpvanderleeden on 15-3-18.
 * POJO for Supplier objects
 */

public class Supplier {
    private int id;
    private String name;
    private String contactLink;

    public Supplier(){ }

    public Supplier(int id) {
        this.id = id;
    }

    public Supplier(String name) { this.name = name; }

    public Supplier(String name, String contactLink) {
        this.name = name;
        this.contactLink = contactLink;}

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactLink() {
        return contactLink;
    }

    public void setContactLink(String contactLink) {
        this.contactLink = contactLink;
    }

    @Override
    public String toString() {
        return "TestSupplier{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", contactLink='" + contactLink + '\'' +
                '}';
    }
}
