package nl.bioinf.iear.model;

/**
 * Created by jpvanderleeden on 15-3-18.
 * POJO for antigen objects
 */

public class Antigen {
    private int id;
    private String name;
    private String description;
    private String geneSymbol;

    public Antigen(String name, String description, String geneSymbol) {
        this.name = name;
        this.description = description;
        this.geneSymbol = geneSymbol;
    }

    public Antigen(String name, String description, String geneSymbol, int id) {
        this.name = name;
        this.description = description;
        this.geneSymbol = geneSymbol;
        this.id = id;
    }

    public Antigen(){ }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getGeneSymbol() {
        return geneSymbol;
    }

    @Override
    public String toString() {
        return "Antigen{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", geneSymbol='" + geneSymbol + '\'' +
                '}';
    }
}
