package nl.bioinf.iear.service;

import nl.bioinf.iear.dao.SuggestionDAO;
import nl.bioinf.iear.model.Suggestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rHut on 02-6-18.
 * Service that implements SuggestionDataSourceJdbc.
 */

@Service
public class SuggestionService {
    private final SuggestionDAO suggestionDAO;

    @Autowired
    public SuggestionService(SuggestionDAO suggestionDAO) {this.suggestionDAO = suggestionDAO; }

    public List<Suggestion> getAllSuggestions() {return suggestionDAO.getAllSuggestions();}

    public List<Suggestion> getAllSuggestionsByType(String suggestionType) {return suggestionDAO.getAllSuggestionsByType(suggestionType);}

    public Suggestion getSuggestionById(int id) {return suggestionDAO.getSuggestionById(id);}

    public Suggestion insertSuggestion(Suggestion suggestion) {return suggestionDAO.insertSuggestion(suggestion);}

    public boolean deleteSuggestion(int id) {return suggestionDAO.deleteSuggestion(id);}
}
