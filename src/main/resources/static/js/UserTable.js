/**
 * Name: ModalControl
 * Version: 0.2.1.8.6.04
 * Author: JvdLeeden, RgHut
 * Use: loading UserDataTable for admins + modal functioning
 */

// Loads the User table and allows for cross script references.
var Alltables = function (){
    var aTable = $('#user_table');
    var table = aTable.DataTable({
        ajax: "data/users",
        ajaxDataProp: "",
        "scrollY": "500px",
        "scrollCollapse": true,
        "paging": true,
        autofill: true,
        columns: tableColumns
    });

    var tableBody = aTable.find('tbody');
    return {
        table: table,
        tableBody: tableBody
    }
}();
