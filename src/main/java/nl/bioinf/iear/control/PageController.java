package nl.bioinf.iear.control;

import nl.bioinf.iear.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;



/**
 * Created by jpvanderleeden on 10-5-18.
 * PageController, Controls all information requests that have a View.
 */

@Controller
public class PageController {

    @Autowired
    private final UserService userService;

    @Autowired
    public PageController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = {"", "/", "/home"}, method = RequestMethod.GET)
    public String homeController(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        String role;

        if (name.equals("anonymousUser")) {
            role = "GUEST";
        } else {
            role = userService.getUserByUserName(name).getRole();
        }
        model.addAttribute("role", role);
        return "home";
    }

    @GetMapping(value = "/user")
    public String userController(){
        return "user";
    }

    @GetMapping(value = "/admin")
    public String adminController(){
        return "admin";
    }









}
